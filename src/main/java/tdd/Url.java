package tdd;

public class Url
{
    private String domain;
    private String protocol;
    private String path;
    private String ip;
    private String query;

    public Url(String url)
    {
        url = url.toLowerCase();

        if (url.equals("") || url == null)
        {
            throw new IllegalArgumentException("URL can not be empty");
        }
        else
        {
            String[] parts = url.split("://");
            setProtocol(parts[0]);

            if (parts.length == 1)
            {
                throw new IllegalArgumentException("URL must have a valid domain.");
            }
            else
            {
                setDomainOrIp(parts[1]);
                setPath(parts[1]);
            }
        }
    }

    /**
     * Strips protocol from url string
     *
     * @param protocol  url to parse
     * @return          String, protocol
     */
    private void setProtocol(String protocol)
    {
        if (protocol.equals("ftp") || protocol.equals("http") || protocol.equals("https") || protocol.equals("file"))
        {
            this.protocol = protocol;
        }
        else
        {
            throw new IllegalArgumentException("Protocol must be one of the following: ftp, http, https, or file.");
        }
    }

    private void setDomainOrIp(String domain)
    {
        String[] parts = domain.split("/", 2);

        if (parts[0].matches("[0-9.]+"))
        {
            ip = parts[0];
            this.domain = "";
        }
        else if (parts[0].equals("") || !parts[0].matches("[a-z0-9.-_]+"))
        {
            throw new IllegalArgumentException("Invalid domain name.");
        }
        else
        {
            this.domain = parts[0];
            ip = "";
        }
    }

    private void setPath(String url)
    {
        String[] parts = url.split("/", 2);
        if (parts.length == 1)
        {
            this.path = "/";
        }
        else
        {
            this.path = "/" + parts[1];
//            parts = parts[1].split("\\?", 2);
        }
    }

    public String getDomain()
    {
        return domain;
    }

    public String getIp()
    {
        return ip;
    }

    public String getProtocol()
    {
        return protocol;
    }

    public String getPath()
    {
        return path;
    }

    public String toString()
    {
        if (!domain.equals(""))
        {
            return protocol + "://" + domain + path;
        }
        else // if (!ip.equals(""))
        {
            return protocol + "://" + ip + path;
        }
    }
}
