package tdd;

import org.junit.Test;
import static org.junit.Assert.*;

public class TestUrl
{
    @Test
    public void testInitDNS()
    {
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("https://launchcode.org/learn", url.toString());
        assertEquals("https", url.getProtocol());
        assertEquals("launchcode.org", url.getDomain());
        assertEquals("/learn", url.getPath());
    }

    @Test
    public void testInitIp()
    {
        Url url = new Url("https://127.0.0.1/");
        assertEquals("https://127.0.0.1/", url.toString());
        assertEquals("https", url.getProtocol());
        assertEquals("127.0.0.1", url.getIp());
        assertEquals("", url.getDomain());
        assertEquals("/", url.getPath());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidProtocol()
    {
        Url url = new Url("bad://launchcode.org/learn");
        fail("Test should not pass");
    }

    @Test
    public void testPath()
    {
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("/learn", url.getPath());

        url = new Url("https://launchcode.org/");
        assertEquals("/", url.getPath());

        url = new Url("https://launchcode.org");
        assertEquals("/", url.getPath());

        url = new Url("https://launchcode.org/dir1/dir2");
        assertEquals("/dir1/dir2", url.getPath());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidDomain1()
    {
        Url url = new Url("https:///learn");
        fail("Test should not pass");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidDomain2()
    {
        Url url = new Url("https://i-am-a-bad-url!.com/learn");
        fail("Test should not pass");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyProtocol()
    {
        Url url = new Url("://google.com/");
        fail("Test should not pass");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDomain1()
    {
        Url url = new Url("https://");
        fail("Test should not pass");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyDomain2()
    {
        Url url = new Url("https:///");
        fail("Test should not pass");
    }

    @Test
    public void testToString()
    {
        Url url = new Url("https://launchcode.org/learn");
        assertEquals("https://launchcode.org/learn", url.toString());

        url = new Url("https://launchcode.org/");
        assertEquals("https://launchcode.org/", url.toString());

        Url ip = new Url("https://127.0.0.1/test");
        assertEquals("https://127.0.0.1/test", ip.toString());

        ip = new Url("https://127.0.0.1/");
        assertEquals("https://127.0.0.1/", ip.toString());
    }
}
